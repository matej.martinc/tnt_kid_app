from lemmagen3 import Lemmatizer
import string
import pandas as pd
from nltk.tokenize import word_tokenize
import random
import re
import pickle
from stopwordsiso import stopwords


def remove_punctuation(text):    
    table = text.maketrans({key: None for key in string.punctuation})
    text = text.translate(table)
    return text

def remove_stopwords(text, lang='et'):
    sw = stopwords(lang)
    for key in sw:
        text.replace(key,"")
    return text

def prepare(text, lang='et'):
    lem_en = Lemmatizer(lang)
    lowered = text.lower()
    no_stopwords = remove_stopwords(lowered)
    sentences = word_tokenize(no_stopwords, language='russian') if lang == 'ru' else word_tokenize(no_stopwords)
    cleaned = [remove_punctuation(sent) for sent in sentences]
    try:
        sentence_lemmatized = ' '.join([lem_en.lemmatize(token) for token in cleaned])
        return sentence_lemmatized
    except Exception as e:
        print(e)
        sentence_lemmatized = ' '.join([token for token in cleaned])
        return sentence_lemmatized

def build_kw(path, lang='et'):
    outs = {}
    sw = stopwords(lang)
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.strip("\n")
            parsed = prepare(line)
            if not parsed in sw:
                outs[parsed] = outs.get(parsed, []) + [line]
    return outs

def find_in_text(text, all_tags):
    found_tags = set()
    for tag in all_tags:
        if tag in text:
            found_tags.union(tag)
    return found_tags

def parse_tags(file):
    data = pd.read_csv(file, encoding='utf-8')
    data = data.fillna(" ")
    return data

def load(lang='et'):
    tf_idf = pickle.load(open("project/tf_idf_files/tf_idf_"+lang+".pickle", "rb"))
    keywords_o = pickle.load(open("project/tf_idf_files/keywords_"+lang+".pickle", "rb"))
    return keywords_o, tf_idf


def extract_kw(vec, features, lemma2kw, threshold=0.0001, multi_strategy="random", n=10):
    """


    Parameters
    ----------
    vec :
        vector with tf_idf values
        tf_idf of weights of the current text
    features : list(str)
        features from the tf_idf.
    lemma2kw : dict
        mapping of lemmas to original keywords.
    multi_strategy : TYPE, optional
        DESCRIPTION. The default is "random".
    n : TYPE, optional
        DESCRIPTION. The default is 10.

    Returns
    -------
    output : TYPE
        DESCRIPTION.

    """
    assert len(vec) == len(features)
    to_sort = []
    for x, y in zip(vec, features):
        if x >= threshold:
            to_sort.append((x, y))
    to_sort.sort(reverse=True)
    output = []
    if multi_strategy == "random":
        for prob, tag in to_sort[:n]:
            possible_tags = lemma2kw[tag]
            if tag in possible_tags:
                new_tag = tag
            else:
                m = len(possible_tags)
                if m > 1 and multi_strategy == "random":
                    new_tag = random.choice(possible_tags)
                elif m > 1 and multi_strategy == "min":
                    new_tag = min(possible_tags, key=len)
                elif m > 1 and multi_strategy == "max":
                    new_tag = max(possible_tags, key=len)
                else:
                    new_tag = possible_tags[0]
            output.append(new_tag)
    return output


def predict_tf_idf(text, lang='et'):
    key,tfidf_vectorizer = load(lang)
    prepared_text = prepare(text, lang)
    vectorized = tfidf_vectorizer.transform([prepared_text])
    get_kw = extract_kw(vectorized.T.todense(), tfidf_vectorizer.get_feature_names(), key)
    return get_kw



