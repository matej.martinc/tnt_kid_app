import argparse
import torch
import sentencepiece as spm
import torch.nn.functional as F
from .preprocessing import get_batch_docs
import sys
from . import model
from . import preprocessing
import pickle


def loadDict(dict_path):
    sys.modules['preprocessing'] = preprocessing
    with open(dict_path, 'rb') as file:
        kw_dictionary = pickle.load(file)
    return kw_dictionary


def loadModel(model_path, cuda):
    sys.modules['model'] = model
    if not cuda:
        kw_model = torch.load(model_path, map_location=torch.device('cpu'))
    else:
        kw_model = torch.load(model_path)
    kw_model.config.cuda = cuda
    if cuda:
        kw_model.cuda()
    else:
        kw_model.cpu()
    return kw_model


def get_tagset(path):
    tagset = {}
    with open(path,'r',encoding='utf-8') as f:
        for line in f:
            line = line.strip()
            if line.lower() in tagset:
                if any(letter.isupper() for letter in line):
                    tagset[line.lower()] = line
            else:
                tagset[line.lower()] = line
    return tagset


def predict(test_data, model, sp, corpus, args, lang, stemmer):
    step = 1
    cut = 0
    all_steps = test_data.size(0)
    encoder_pos = None
    total_pred = []

    with torch.no_grad():

        for i in range(0, all_steps - cut, step):

            encoder_words = get_batch_docs(test_data, i, args['cuda'])
            logits = model(encoder_words, input_pos=encoder_pos, lm_labels=None, predict=True)
            maxes = []

            batch_counter = 0

            for batch in logits:

                pred_example = []
                batch = F.softmax(batch, dim=1)
                length = batch.size(0)
                position = 0

                probs_dict = {}

                while position < len(batch):
                    pred = batch[position]

                    _, idx = pred.max(0)
                    idx = idx.item()

                    if idx == 1:
                        words = []
                        num_steps = length - position
                        for j in range(num_steps):

                            new_pred = batch[position + j]

                            values, new_idx = new_pred.max(0)
                            new_idx = new_idx.item()
                            prob = values.item()

                            if new_idx == 1:
                                word = corpus.dictionary.idx2word[encoder_words[batch_counter][position + j]]
                                words.append((word, prob))

                                # add max word prob in document to prob dictionary
                                if lang == 'english':
                                    stem = stemmer.stem(word)
                                elif lang == 'estonian' or lang=='russian':
                                    stem = stemmer.lemmatize(word)

                                if stem not in probs_dict:
                                    probs_dict[stem] = prob
                                else:
                                    if probs_dict[stem] < prob:
                                        probs_dict[stem] = prob
                            else:
                                if args['bpe']:
                                    word = corpus.dictionary.idx2word[encoder_words[batch_counter][position + j]]
                                    if not word.startswith('▁'):
                                        words = []
                                break

                        position += j + 1
                        words = [x[0] for x in words]
                        if args['bpe']:
                            if len(words) > 0 and words[0].startswith('▁'):
                                pred_example.append(words)
                        else:
                            pred_example.append(words)
                    else:
                        position += 1

                # assign probabilities
                pred_examples_with_probs = []
                for kw in pred_example:
                    probs = []
                    for word in kw:
                        if lang == 'english':
                            stem = stemmer.stem(word)
                        elif lang == 'estonian' or lang=='russian':
                            stem = stemmer.lemmatize(word)
                        probs.append(probs_dict[stem])

                    kw_prob = sum(probs) / len(probs)
                    pred_examples_with_probs.append((" ".join(kw), kw_prob))

                pred_example = pred_examples_with_probs

                # sort by softmax probability
                pred_example = sorted(pred_example, reverse=True, key=lambda x: x[1])

                # remove keywords that contain punctuation and duplicates
                all_kw = set()
                filtered_pred_example = []
                kw_stems = []

                punctuation = "!#$%&'()*+,.:;<=>?@[\]^_`{|}~"

                for kw, prob in pred_example:
                    if lang == 'english':
                        kw_stem = " ".join([stemmer.stem(word) for word in kw.split()])
                    elif lang == 'estonian' or lang=='russian':
                        kw_stem = " ".join([stemmer.lemmatize(word) for word in kw.split()])
                    kw_stems.append(kw_stem)

                    if kw_stem not in all_kw and len(kw_stem.split()) == len(set(kw_stem.split())):
                        has_punct = False
                        for punct in punctuation:
                            if punct in kw:
                                has_punct = True
                                break
                        if sp is not None:
                            kw_decoded = sp.DecodePieces(kw.split())
                            if not has_punct and len(kw_decoded.split()) < 5:
                                filtered_pred_example.append((kw, prob))
                        else:
                            if not has_punct and len(kw.split()) < 5:
                                filtered_pred_example.append((kw, prob))
                    all_kw.add(kw_stem)

                pred_example = filtered_pred_example
                filtered_pred_example = [x[0] for x in pred_example][:args['kw_cut']]

                maxes.append(filtered_pred_example)
                batch_counter += 1

            if sp is not None:
                all_decoded_maxes = []
                for doc in maxes:
                    decoded_maxes = []
                    for kw in doc:
                        kw = sp.DecodePieces(kw.split())
                        decoded_maxes.append(kw)
                    all_decoded_maxes.extend(decoded_maxes)

                maxes = all_decoded_maxes

            total_pred.extend(maxes)

    if args['split_docs']:
        total_kw = set()
        filtered_total_pred = []

        for kw in total_pred:
            if lang == 'english':
                kw_stem = " ".join([stemmer.stem(word) for word in kw.split()])
            elif lang == 'estonian' or lang=='russian':
                kw_stem = " ".join([stemmer.lemmatize(word) for word in kw.split()])
            if kw_stem not in total_kw:
                filtered_total_pred.append(kw)
            total_kw.add(kw_stem)
        total_pred = filtered_total_pred
    return total_pred


