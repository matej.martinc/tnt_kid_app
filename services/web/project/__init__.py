import os
import json

from flask import (
    Flask,
    jsonify,
    send_from_directory,
    request,
    redirect,
    url_for
)

from flask_restx import Api, Resource, fields, abort, reqparse
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from werkzeug.utils import secure_filename
from werkzeug.middleware.proxy_fix import ProxyFix

from . import api_functions
from . import keyword_extraction_main as kw

from nltk.stem.porter import *

from lemmagen3 import Lemmatizer

lemmatizer_et = Lemmatizer('et')
lemmatizer_ru = Lemmatizer('ru')


app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0',
          title='API services',
          description='TNT-KID Estonian keyword extractor REST API')
ns = api.namespace('rest_api', description='REST services API')

args = {
    'max_length': 256,
    'cuda': False,
    'kw_cut': 10,
    'split_docs': True,   
    'bpe': True, 
}

kw_et_model = kw.loadModel('project/trained_classification_models/model_5_lm+bpe+rnn_estonian_big_folder_estonian_loss_1.7198098886227178_epoch_9.pt', args['cuda'])
kw_et_dictionary = kw.loadDict('project/dictionaries/dict_5_lm+bpe+rnn_estonian_big_adaptive_lm_bpe_nopos_rnn_nocrf.ptb')
tagset_et = kw.get_tagset('project/tf_idf_files/Ekspress_tagid-latin.csv')
kw_sp_et = kw.spm.SentencePieceProcessor()
kw_sp_et.Load('project/bpe/bpe_estonian_big.model')

kw_ru_model = kw.loadModel('project/trained_classification_models/model_5_lm+bpe+rnn_russian_big_folder_russian_loss_0.764110385809901_epoch_8.pt', args['cuda'])
kw_ru_dictionary = kw.loadDict('project/dictionaries/dict_5_lm+bpe+rnn_russian_big_adaptive_lm_bpe_nopos_rnn_nocrf.ptb')
tagset_ru = kw.get_tagset('project/tf_idf_files/Ekspress_tagid-cyrl.csv')
kw_sp_ru = kw.spm.SentencePieceProcessor()
kw_sp_ru.Load('project/bpe/bpe_russian_big.model')


# input and output definitions
kw_extractor_input = api.model('KeywordExtractorInput', {
    'text': fields.String(required=True, description='Title + lead + body of the article'),
    'language': fields.String(required=False, description='Language, can be "estonian" or "russian"'),
})

kw_extractor_output = api.model('KeywordExtractorOutput', {
    'keywords_in_tagset': fields.List(fields.String, description='Extracted keywords that are present in the existing tagset'),
    'keywords_new': fields.List(fields.String, description='Extracted keywords that are not present in the existing tagset')
})


@ns.route('/extract_keywords/')
class KeywordExtractor(Resource):
    @ns.doc('Extracts keywords from news article')
    @ns.expect(kw_extractor_input, validate=True)
    @ns.marshal_with(kw_extractor_output)
    def post(self):
        if 'language' not in api.payload or api.payload['language'] not in ['estonian', 'russian']:
            language = api_functions.get_language(api.payload['text'])
        else:
            language = api.payload['language']

        if language== 'russian':
            kw_in_text, kw_new = api_functions.extract_keywords(api.payload['text'], kw_ru_model, kw_ru_dictionary, kw_sp_ru, lemmatizer_ru, tagset_ru, args, 'russian')
        else:
            kw_in_text, kw_new = api_functions.extract_keywords(api.payload['text'], kw_et_model, kw_et_dictionary, kw_sp_et, lemmatizer_et, tagset_et, args, 'estonian')
        return {"keywords_in_tagset": kw_in_text, "keywords_new": kw_new}


@ns.route('/health/')
class Health(Resource):
    @ns.response(200, "successfully fetched health details")
    def get(self):
        return {"status": "running", "message": "Health check successful"}, 200, {}
